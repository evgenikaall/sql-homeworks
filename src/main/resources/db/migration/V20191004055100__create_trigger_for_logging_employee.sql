CREATE OR REPLACE TRIGGER log_employees_entry
    AFTER INSERT OR DELETE
    ON employees
    FOR EACH ROW
BEGIN
    IF INSERTING THEN
        fill_employees_log(:NEW.employee_id, :NEW.first_name, :NEW.last_name, 'HIRED');
    ELSIF DELETING THEN
        fill_employees_log(:OLD.employee_id, :OLD.first_name, :OLD.last_name, 'FIRED');
    END IF;
END log_employees_entry;