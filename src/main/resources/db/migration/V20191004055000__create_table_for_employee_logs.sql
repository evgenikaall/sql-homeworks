CREATE TABLE employment_logs(
    employment_log_id NUMBER(6)           NOT NULL,
    first_name        VARCHAR2(20)                ,
    last_name         VARCHAR2(25)                ,
    employment_action CHAR(5)  NOT NULL CHECK(employment_action = 'HIRED' OR employment_action = 'FIRED'),
    employment_status_updtd_tmstmp TIMESTAMP
);

CREATE OR REPLACE PROCEDURE fill_employees_log(id IN NUMBER, fn IN VARCHAR2, ln IN VARCHAR2, message IN VARCHAR2)
AS
BEGIN
    INSERT INTO employment_logs (employment_log_id, first_name, last_name, employment_action, employment_status_updtd_tmstmp)
                         VALUES (id,fn, ln, message, SYSTIMESTAMP);

END fill_employees_log;
