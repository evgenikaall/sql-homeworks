-- for inserting into job_histroy, I must fill their parent tables
-- and parent tables for parent tables


-- insert into regions
INSERT INTO regions (region_id, region_name)
             VALUES (1, 'EUROPE');

-- insert into countries
INSERT INTO COUNTRIES (country_id, country_name, region_id)
               VALUES ('FR', 'FRANCE', 1);

-- insert into locations
INSERT INTO locations (location_id,street_address, postal_code, city, state_province, country_id)
               VALUES (1000, 'Champ de Mars, 5 Avenue Anatole France', '7500', 'PARIS', NULL, 'FR');

-- insert into departments
INSERT INTO departments (department_id, department_name, manager_id, location_id)
                 VALUES (1100, 'Baker"s', NULL, 1000);

-- insert into jobs
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
          VALUES ('BK_CH', 'Baker"s chief', 4000, 10000);

-- insert into employees
INSERT INTO employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id)
               VALUES (100000, 'John', 'Johnov', 'kptn.johnnman@gmail.com', '+37366633312', '08-apr-2021', 'BK_CH', 8000, NULL, NULL, 1100 );

-- fill department's manager
UPDATE departments
SET manager_id = 100000
WHERE department_id = 1100;

-- insert into job_history (main aim)
INSERT INTO job_history (employee_id, start_date, end_date, job_id, department_id)
                 VALUES (100000, '09-feb-2020', '06-may-2022', 'BK_CH', 1100);
