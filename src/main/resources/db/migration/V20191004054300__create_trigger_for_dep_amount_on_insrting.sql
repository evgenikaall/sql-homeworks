CREATE OR REPLACE TRIGGER recount_department_amount_on_insert
    AFTER INSERT OR DELETE
    ON departments
    FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE locations
        SET  department_amount = department_amount + 1
        WHERE location_id = :NEW.location_id;
    ELSIF DELETING THEN
        UPDATE locations
        SET  department_amount = department_amount - 1
        WHERE location_id = :OLD.location_id;
    END IF;
END recount_department_amount_on_insert;
