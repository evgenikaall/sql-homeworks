ALTER TABLE locations
ADD department_amount NUMBER DEFAULT 0;

UPDATE locations
SET department_amount = (
                         SELECT COUNT(*)
                         FROM departments d
                         WHERE d.location_id = locations.location_id
                        );

COMMENT ON COLUMN locations.department_amount
IS 'Contains the amount of departments in the location';
