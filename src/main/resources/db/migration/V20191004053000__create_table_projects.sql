-- create table projects
CREATE TABLE projects (
    project_id NUMBER(6) PRIMARY KEY CHECK(project_id > 0),
    project_description VARCHAR(30) CHECK(project_description > 10),
    project_investments NUMBER(8, -3),
    project_revenue NUMBER(8,2)
);

CREATE TABLE projects_employees (
    project_id NUMBER(6) NOT NULL CHECK(project_id > 0),
    employee_id NUMBER(6) NOT NULL,
    amount_of_hours NUMBER(5) CHECK(amount_of_hours > 0)
);

ALTER TABLE projects_employees
ADD CONSTRAINT PE_PROJ_FK FOREIGN KEY(project_id) REFERENCES projects(project_id);

ALTER TABLE projects_employees
ADD CONSTRAINT PE_EMP_FK FOREIGN KEY(employee_id) REFERENCES employees(employee_id);