-- create pay table

CREATE TABLE pay (
    cardNr         VARCHAR2(12) PRIMARY KEY,
    salary         NUMBER(8, 2),
    commission_pct NUMBER(2, 2)
);

-- add cardNr into employees with relation 1:1
ALTER TABLE employees
ADD cardNr VARCHAR(12) UNIQUE;

-- set constraint
ALTER TABLE employees
ADD CONSTRAINT EMP_PAY_FK FOREIGN KEY (cardNr)
REFERENCES pay(cardNr);

-- fill salary
UPDATE pay
SET pay.salary = (
             SELECT e.salary
             FROM employees e
             WHERE e.cardNr = pay.cardNr
             );

-- fill commission_pct
UPDATE pay
SET pay.commission_pct = (
             SELECT e.commission_pct
             FROM employees e
             WHERE e.cardNr = pay.cardNr
             );