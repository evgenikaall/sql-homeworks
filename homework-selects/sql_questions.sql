-- Write a query to display: 
-- 1. the first name, last name, salary, and job grade for all employees.
SELECT first_name, last_name, salary, job_title
FROM employees
LEFT JOIN jobs USING (job_id);

-- 2. the first and last name, department, city, and state province for each employee.
SELECT first_name ||' '||last_name AS "full_name", department_name, city, state_province
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN locations l
ON l.location_id = d.location_id;

-- 3. the first name, last name, department number and department name, for all employees for departments 80 or 40.
SELECT e.first_name,last_name, e.department_id||' '||department_name AS "department"
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE d.department_id IN (40, 80);

-- 4. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
SELECT first_name,'contain letter z ' AS "MESSAGE OF QUERY", last_name, d.department_name, city, state_province
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN locations l
ON d.location_id = l.location_id
WHERE UPPER(first_name) LIKE '%Z%';

-- 5. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
SELECT first_name ||' '||last_name AS "full_name", salary
FROM employees e
WHERE e.salary < (
                 SELECT salary
                 FROM employees e1
                 WHERE e1.employee_id = 182
                 );

-- 6. the first name of all employees including the first name of their manager.
SELECT e.first_name "employee", m.first_name "manager"
FROM employees e
INNER JOIN employees m
ON m.employee_id = e.manager_id;

-- 7. the first name of all employees and the first name of their manager including those who does not working under any manager.
SELECT e.first_name "employee", m.first_name "manager"
FROM employees e
LEFT JOIN employees m
ON m.employee_id = e.manager_id;

-- 8. the details of employees who manage a department.
SELECT first_name ||' '||last_name AS "full_name", email, phone_number,job_title, d.department_name
FROM employees e
INNER JOIN departments d
ON e.employee_id = d.manager_id
INNER JOIN jobs j
ON e.job_id = j.job_id;

-- 9. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
SELECT e.first_name, e.last_name, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.department_id IN (
                          SELECT e1.department_id
                          FROM employees e1
                          INNER JOIN departments d1
                          ON e1.department_id = d1.department_id
                          WHERE UPPER(last_name) = 'TAYLOR'
                        );

--10. the department name and number of employees in each of the department.
SELECT COUNT(*) "counter", d.department_name
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
GROUP BY d.department_name;

--11. the name of the department, average salary and number of employees working in that department who got commission.
SELECT d.department_name, AVG(salary) "AVERAGE SALARY ", COUNT(*) "COUNTER"
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.commission_pct IS NOT NULL
GROUP BY d.department_name;

--12. job title and average salary of employees.
SELECT j.job_title, AVG(salary) "AVERAGE SALARY"
FROM employees e
INNER JOIN jobs j
ON j.job_id = e.job_id
GROUP BY j.job_title;

--13. the country name, city, and number of those departments where at least 2 employees are working.
SELECT c.country_name, city, COUNT(*) "COUNTER OF DEPARTMENTS"
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN locations l
ON l.location_id = d.location_id
INNER JOIN countries c
ON l.country_id = c.country_id
GROUP BY c.country_name, city
HAVING COUNT(*) > 2;

--14. the employee ID, job name, number of days worked in for all those jobs in department 80.
SELECT e.employee_id, j.job_title, TO_DATE(j_h.end_date)-TO_DATE(j_h.start_date) "WORK DAYS"
FROM employees e
INNER JOIN job_history j_h
ON j_h.employee_id = e.employee_id
INNER JOIN departments d
ON j_h.department_id = d.department_id
INNER JOIN jobs j
ON j_h.job_id = j.job_id
WHERE d.department_id = 80;

--15. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
SELECT e.first_name ||' '||e.last_name AS "full_name"
FROM employees e
WHERE e.salary > (
                 SELECT e1.salary
                 FROM employees e1
                 WHERE e1.employee_id = 163
                 );

--16. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary.
SELECT e.employee_id, first_name ||' '||last_name AS "full_name"
FROM employees e
WHERE e.salary > (
                 SELECT AVG(e1.salary)
                 FROM employees e1
                 );

--17. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam.
SELECT empl.first_name ||' '||empl.last_name AS "full_name", empl.employee_id, empl.salary
FROM employees empl
INNER JOIN employees mg
ON empl.manager_id = mg.employee_id
WHERE UPPER(mg.first_name) = 'PAYAM';

--18. the department number, name ( first name and last name ), job and department name for all employees in the Finance department.
SELECT d.department_id, e.first_name ||' '||e.last_name AS "full_name", j.job_title, d.department_name
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN jobs j
ON j.job_id = e.job_id
WHERE UPPER(d.department_name) = 'FINANCE';

--19. all the information of an employee whose id is any of the number 134, 159 and 183.
SELECT *
FROM employees
WHERE employee_id IN (134, 159, 183);

--20. all the information of the employees whose salary is within the range of smallest salary and 2500.
SELECT *
FROM employees
WHERE salary BETWEEN (SELECT MIN(salary) FROM employees) AND 2500;

--21. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200.
SELECT *
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.department_id NOT IN (
                             SELECT d1.department_id
                             FROM employees e1
                             INNER JOIN departments d1
                             ON e1.department_id = d1.department_id
                             WHERE e1.employee_id BETWEEN 100 AND 200
                             );

--22. all the information for those employees whose id is any id who earn the second highest salary.
SELECT *
FROM employees
WHERE SALARY IN (
                 SELECT MAX(e1.salary)
                 FROM employees e1
                 WHERE e1.salary NOT IN (
                                      SELECT MAX(e2.SALARY)
                                      FROM employees e2
                                      )
                );

--23. the employee name( first name and last name ) and hiredate for all employees in the same department as Clara. Exclude Clara.
SELECT first_name||' '||last_name "WORKING IN DEPARTMENT, WHERE IS WORKING CLARA"
FROM employees
WHERE UPPER(first_name) != 'CLARA' AND department_id IN (
                                                         SELECT e2.department_id
                                                         FROM employees e2
                                                         WHERE UPPER(e2.first_name) = 'CLARA'
                                                         );

--24. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T.
SELECT employee_id, first_name||' '||last_name "full_name"
FROM employees
WHERE department_id IN (
                        SELECT e2.department_id
                        FROM employees e2
                        WHERE UPPER(e2.first_name) LIKE '%T%' OR UPPER(last_name) LIKE '%T%'
                        );

--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
SELECT e.first_name||' '||e.last_name "full_name", j.job_title, j_h.start_date, j_h.end_date
FROM employees e
INNER JOIN job_history j_h
ON j_h.employee_id = e.employee_id
INNER JOIN jobs j
ON j_h.job_id = j.job_id
WHERE e.commission_pct IS NULL;

--26. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a J in their name.
SELECT employee_id, first_name||' '||last_name "full_name", salary
FROM employees
WHERE employee_id IN (
                      SELECT e2.employee_id
                      FROM employees e2
                      WHERE (UPPER(e2.first_name) LIKE '%J%' OR UPPER(e2.last_name) LIKE '%J%')
                            AND e2.salary > (
                                          SELECT AVG(e3.salary)
                                          FROM employees e3
                                         )
                    );

--27. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. (???????? ? ???????)
SELECT e.employee_id, e.first_name||' '||e.last_name "full_name", j.job_title
FROM employees e
INNER JOIN jobs j
ON j.job_id = e.job_id
WHERE salary < (
                SELECT e1.salary
                FROM employees e1
                INNER JOIN jobs j1
                ON j1.job_id = e1.job_id
                WHERE UPPER(j1.job_id) = 'MK_MAN'
                );

--28. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. Exclude Job title MK_MAN. (????? ????????, ?? ????? ?? ???????)
SELECT e.employee_id, e.first_name||' '||e.last_name "full_name", j.job_title
FROM employees e
INNER JOIN jobs j
ON j.job_id = e.job_id
WHERE UPPER(j.job_id) != 'MK_MAN' AND salary < (
                SELECT e1.salary
                FROM employees e1
                INNER JOIN jobs j1
                ON j1.job_id = e1.job_id
                WHERE UPPER(j1.job_id) = 'MK_MAN'
                );

--29. all the information of those employees who did not have any job in the past.
SELECT *
FROM employees e
LEFT JOIN job_history j_h
ON e.employee_id = j_h.employee_id
WHERE j_h.start_date IS NULL AND j_h.end_date IS NULL;

--30. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.
SELECT e.employee_id, e.first_name||' '||e.last_name "full_name", j.job_title
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN jobs j
ON e.job_id = j.job_id
WHERE salary > any(
                   SELECT AVG(e1.salary)
                   FROM employees e1
                   INNER JOIN departments d1
                   ON e1.department_id = d1.department_id
                   GROUP BY d1.department_name
                  );

--31. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG. ( ?? ?? ???????? )
SELECT e.employee_id,  e.first_name||' '||e.last_name "full_name",
CASE UPPER(j.job_id)
WHEN 'ST_MAN' THEN 'SALESMAN'
WHEN 'IT_PROG' THEN 'DEVELOPER'
END AS "job_title"
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
INNER JOIN jobs j
ON e.job_id = j.job_id
WHERE UPPER(j.job_id) IN ('ST_MAN', 'IT_PROG');

--32. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.
SELECT e.employee_id,  e.first_name||' '||e.last_name "full_name", salary,
CASE
WHEN salary > (SELECT AVG(e1.salary) FROM employees e1) THEN 'HIGH'
ELSE 'LOW'
END AS "SalaryStatus"
FROM employees e;

--33. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees)
    -- and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than
    -- the average salary of all employees.
SELECT e.employee_id,  e.first_name||' '||e.last_name "full_name", salary "SalaryDrawn",
(SELECT AVG(salary) FROM employees) AS "AvgCompare",
CASE
WHEN salary > (SELECT AVG(e1.salary) FROM employees e1) THEN 'HIGH'
ELSE 'LOW'
END AS "SalaryStatus"
FROM employees e;


--34. all the employees who earn more than the average and who work in any of the IT departments.
SELECT *
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE UPPER(d.department_name) = 'IT' AND salary > (SELECT AVG(e1.salary) FROM employees e1);

--35. who earns more than Mr. Ozer.
SELECT e.first_name||' '||e.last_name "full_name", 'Ms.Ozer' "EARNS_MORE_THEN"
FROM employees e
WHERE salary > (
                SELECT e1.salary
                FROM employees e1
                WHERE UPPER(e1.last_name) = 'OZER'
                );

--36. which employees have a manager who works for a department based in the US.
SELECT e.first_name||' '||e.last_name "Manager of", l.country_id "Based in the"
FROM employees e
INNER JOIN departments d
ON d.manager_id = e.manager_id
INNER JOIN locations l
ON l.location_id = d.location_id
WHERE UPPER(l.country_id) = 'US';

--37. the names of all employees whose salary is greater than 50% of their department’s total salary bill.
SELECT *
FROM employees e1
WHERE salary > (
                SELECT (SUM(e2.salary)*0.5)
                FROM employees e2
                WHERE e1.department_id = e2.department_id
                );

--38. the employee id, name ( first name and last name ), salary, department name and city for all
--the employees who gets the salary as the salary earn by the employee which is maximum within the
--joining person January 1st, 2002 and December 31st, 2003.
SELECT e.employee_id, e.first_name||' '||e.last_name "name", e.salary, d.department_name, l.city
FROM employees e
LEFT JOIN departments d
ON e.department_id = d.department_id
LEFT JOIN locations l
ON l.location_id = d.location_id
WHERE e.salary = (
               SELECT MAX(e1.salary)
               FROM employees e1
               WHERE e1.hire_date BETWEEN '01-jan-2002' AND '12-dec-2003'
              );

--39. the first and last name, salary, and department ID for all those employees who earn more than the average salary and arrange the list in descending order on salary.
SELECT e.employee_id, e.first_name||' '||e.last_name "name", e.salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE salary > (SELECT AVG(e1.salary) FROM employees e1)
ORDER BY salary DESC;

--40. the first and last name, salary, and department ID for those employees who earn more than the maximum salary of a department which ID is 40.
SELECT e.first_name||' '||e.last_name "full_name", salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE salary > (
                SELECT MAX(e1.salary)
                FROM employees e1
                INNER JOIN departments d1
                ON e1.department_id = d1.department_id
                WHERE d1.department_id = 40
                );

--41. the department name and Id for all departments where they located, that Id is equal to the Id for the location where department number 30 is located.
SELECT d.department_name||' '||d.department_id "department", l.location_id "LOCATED IN"
FROM departments d
INNER JOIN locations l
ON d.location_id = l.location_id
WHERE l.location_id IN (
                        SELECT l1.location_id
                        FROM departments d1
                        INNER JOIN locations l1
                        ON d1.location_id = l1.location_id
                        WHERE d1.department_id = 30
                        );


--42. the first and last name, salary, and department ID for all those employees who work in that department where the employee works who hold the ID 201.
SELECT e.first_name||' '||e.last_name "full_name", salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE d.department_id IN (
                          SELECT d1.department_id
                          FROM employees e1
                          INNER JOIN departments d1
                          ON e1.department_id = d1.department_id
                          WHERE e1.employee_id = 201
                         );

--43. the first and last name, salary, and department ID for those employees whose salary is equal to the salary of the employee who works in that department which ID is 40.
SELECT e.first_name||' '||e.last_name "full_name", salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.salary = (
                  SELECT e1.salary
                  FROM employees e1
                  INNER JOIN departments d1
                  ON e1.department_id = d1.department_id
                  WHERE e1.department_id = 40
                 );

--44. the first and last name, salary, and department ID for those employees who earn more than the minimum salary of a department which ID is 40.
SELECT e.first_name||' '||e.last_name "full_name", salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.salary > (
                  SELECT MIN(e1.salary)
                  FROM employees e1
                  INNER JOIN departments d1
                  ON e1.department_id = d1.department_id
                  WHERE e1.department_id = 40
                 );

--45. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70.
SELECT e.first_name||' '||e.last_name "full_name", salary, d.department_id
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.salary < (
                  SELECT MIN(e1.salary)
                  FROM employees e1
                  INNER JOIN departments d1
                  ON e1.department_id = d1.department_id
                  WHERE e1.department_id = 70
                 );

--46. the first and last name, salary, and department ID for those employees who earn less than the average salary,
--and also work at the department where the employee Laura is working as a first name holder.
SELECT e.first_name||' '||e.last_name "full_name", e.salary, e.department_id
FROM employees e
WHERE ( e.department_id IN (
                        SELECT e1.department_id
                        FROM employees e1
                        WHERE UPPER(e1.first_name) = 'LAURA'
                        ))
AND
      ( e.employee_id IN (
                        SELECT e2.employee_id
                        FROM employees e2
                        WHERE salary < (SELECT AVG(salary) FROM employees)
                        ));

--47. the full name (first and last name) of manager who is supervising 4 or more employees.
SELECT m.first_name||' '||m.last_name "MANAGER", COUNT(*) "SUPERVISING"
FROM employees e
INNER JOIN employees m
ON m.employee_id = e.manager_id
GROUP BY m.first_name, m.last_name
HAVING COUNT(*) > 4;

--48. the details of the current job for those employees who worked as a Sales Representative in the past.
SELECT e.job_id, j.job_title, e.first_name||' '||e.last_name "name"
FROM employees e
INNER JOIN jobs j
ON j.job_id = e.job_id
WHERE e.employee_id IN (
                        SELECT e1.employee_id
                        FROM employees e1
                        INNER JOIN job_history j_h
                        ON j_h.employee_id = e1.employee_id
                        INNER JOIN jobs j1
                        ON j1.job_id = j_h.job_id
                        WHERE UPPER(j1.job_title) = 'SALES REPRESENTATIVE'
                       );

--49. all the infromation about those employees who earn second lowest salary of all the employees.
SELECT *
FROM employees
WHERE salary IN (
                 SELECT MIN(salary)
                 FROM employees
                 WHERE salary NOT IN (
                                      SELECT MIN(SALARY)
                                      FROM employees
                                      )
                );

--50. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department.
SELECT e.department_id, e.first_name||' '||e.last_name "full_name", e.salary
FROM employees e
WHERE (e.department_id IN (
                        SELECT e1.department_id
                        FROM employees e1
                        GROUP BY e1.department_id
                       ))
AND
              (e.salary = (
                        SELECT MAX(salary)
                        FROM employees e2
                        WHERE e2.department_id = e.department_id
                       ));